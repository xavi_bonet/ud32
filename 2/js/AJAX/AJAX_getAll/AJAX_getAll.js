addEventListener('load',inicializarEventos, false);

function inicializarEventos() {
    getAll();
}

function getAll() {
    $.ajax({
        url: "https://restcountries.eu/rest/v2/name/Afghanistan",
        method: 'GET',
        dataType: 'json',
        headers: {
            'Accept':'application/json',
        },
        contentType: 'application/x-www-form-urlencoded',
        success: function (data) {
            console.log(JSON.stringify(data))
        },
        error: function (error) {
            console.log(error);
        }
    });
}