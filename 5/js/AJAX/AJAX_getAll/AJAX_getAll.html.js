$( document ).ready(function() {

    $.ajax({
        url: 'https://restcountries.eu/rest/v2/name/Afghanistan?fullText=true',
        method: 'GET',
        dataType: 'json',
        headers: {
            'Accept':'application/json',
        },
        contentType: 'application/x-www-form-urlencoded',
        success: function(respuesta) {
            console.log(respuesta);
        },
        error: function(error) {
            console.log(error);
        }
    });

});
